# -*- coding: utf-8 -*-
from keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from keras_retinanet import models
import cv2
import os
import glob
from tqdm import tqdm
import numpy as np
import time
from retina.utils import visualize_boxes
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('AGG')


MODEL_PATH = 'snapshots/resnet152_pascal_05.h5'
IMAGE_PATH = '../test/test/3.png'


def load_inference_model(model_path=os.path.join('snapshots', 'resnet.h5')):
    model = models.load_model(model_path, backbone_name='resnet152')
    model = models.convert_model(model)
    model.summary()
    return model


def post_process(boxes, original_img, preprocessed_img):
    # post-processing
    h, w, _ = preprocessed_img.shape
    h2, w2, _ = original_img.shape
    boxes[:, :, 0] = boxes[:, :, 0] / w * w2
    boxes[:, :, 2] = boxes[:, :, 2] / w * w2
    boxes[:, :, 1] = boxes[:, :, 1] / h * h2
    boxes[:, :, 3] = boxes[:, :, 3] / h * h2
    return boxes


if __name__ == '__main__':

    save_dname = "./results_img"
    model = load_inference_model(MODEL_PATH)

    list_dict = []

    imgs_fnames = glob.glob(os.path.join(".", "*.png"))
    imgs_fnames = glob.glob(os.path.join(
        "../preprocessed_img/test/test", "*.png"))
    imgs_fnames = sorted(imgs_fnames, key=lambda x: int(
        (x.split('/')[4]).split('.')[0]))
    print(imgs_fnames)
    for image_path in tqdm(imgs_fnames):
        # 2. Load image
        # load image
        image = read_image_bgr(image_path)

        # copy to draw on
        draw = image.copy()
        draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

        # preprocess image for network
        image = preprocess_image(image)
        image, _ = resize_image(image, 416, 448)

        # process image
        boxes, scores, labels = model.predict_on_batch(
            np.expand_dims(image, axis=0))

        boxes = post_process(boxes, draw, image)
        labels = labels[0]
        scores = scores[0]
        boxes = boxes[0].tolist()

        # get valid len
        valid_len = len(scores[scores >= .5])

        visualize_boxes(draw, boxes, labels, scores, class_labels=[
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])

        # dict
        ordered_boxes = [[b[1], b[0], b[3], b[2]]
                         for b in boxes]  # y1, x1, y2, x2
        my_dict = {"bbox": ordered_boxes[:valid_len], "label": [
            int(10) if l == 0 else int(l) for l in labels[:valid_len]], "score": [
            float(l) for l in scores[:valid_len]]}
        list_dict.append(my_dict)

        # 5. plot
        output_path = os.path.join(save_dname, os.path.split(image_path)[-1])
        cv2.imwrite(output_path, draw[:, :, :: -1])
        # plt.imshow(image)
        # plt.show()

    # save dict
    import json
    save_dict_path = './result.json'
    with open(save_dict_path, 'w') as fp:
        json.dump(list_dict, fp)
